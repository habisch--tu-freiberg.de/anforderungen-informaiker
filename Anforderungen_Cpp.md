# Anforderungskatalog an C, C++, C\# - Programmierer

## Basic-/Grundlagen- Level

### Allgemeinwissen
- Wie sind Programme allgemein aufgebaut? (Preambel, usw.)
- Wie wird ein Programm übersetzt? Vorführung anhand eines kleinen praktischen Beispiels.

### Sprachspezifisches Wissen
- Was ist eine Referenz? Wann wird sie eingesetzt?
- Was ist ein Pointer? einfaches Beispiel
- Wie unterscheiden sich Referenzen und Pointer?
- Was ist dynamische Speicherverwaltung? (mindestens new/delete, malloc/free - besser SmartPointer)

### Objektorientierte Programmierung
- Prinzipien der objekt-orientierten Programmierung, wie Hirachie, Abstraktion oder Modularität 
- Techniken: Komposition/Aggregation, Vererbung, Polymorphie, Virtuelle Methoden, Kapselung
